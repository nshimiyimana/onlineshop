const Joi=require('joi');
 Joi.objectId=require('joi-objectid')(Joi);


const mongoose=require('mongoose');

const express=require('express');
const app=express();
require('./src/startup/routes')(app);

mongoose.connect('mongodb://localhost/online_shop')
.then(()=>console.log('successfullly connected to mongodb'))
.catch(()=>console.log('unable to establish connection with mongodb'));



const port=process.env.NODE_ENV || 2000;
app.listen(port,()=>console.log('listens port'+port+ ' now'))



const {Supplier,validate}=require('../modal/supplier');

const express=require('express');
const router=express.Router();


router.post('/',async (req,res)=>{

const {error}=validate(req.body);
if(error) return res.status(400).send(error.details[0].message);


let supplier=new Supplier({
names:req.body.names,
email:req.body.email,
business_name:req.body.business_name,
phone:req.body.phone,
city:req.body.city,
country:req.body.country

});

supplier=await supplier.save();
res.send(supplier);


});

router.get('/',async (req,res)=>{
let supplier=Supplier.find();
res.send(supplier);


});


module.exports=router;
const {Account}=require('../modal/account');

const express=require('express');



const router=express.Router();


router.post('/',async(req,res)=>{


    let account=new Account({
username:req.body.username,
password:req.body.password,
email:req.body.email,
user_image:req.body.user_image,
isAdmin:req.body.isAdmin,
operations:req.body.operations
    });
try{

    account=await account.save();
    res.send(account);
}
catch(err){
    console.log(err.message);
    res.send(err.message);
}

});


router.get('/:id',async(req,res)=>{

    const account=await Account.findById(req.params.id);
    if(!account) return res.status(404).send('please account not found')
res.send(account);



});

router.get('/',async(req,res)=>{

    const account=await Account.findById();
    
res.send(account);



});
router.delete('/:id',async(req,res)=>{

    let account=await Account.findByIdAndRemove(req.params.id);
    if(!account) return res.status(404).send('please account with the given id is not found');
    try{

    account=await account.remove();
    res.send(account);
    }

    catch(err){
        console.log(err.message);
        res.send(err.message);
    }
    
})




module.exports =router;

const {Product,validate}=require('../modal/product');

const {Supplier}=require('../modal/supplier');
const {Category}=require('../modal/categories');
const {Account}=require('../modal/account');

const express=require('express');
const { Customer } = require('../modal/customers');
const router=express.Router();


router.post('/',async (req,res)=>{

    const {error}=validate(req.body);
    if(error) return res.status(400).send(error.details[0].message);
    let categories=await Category.findById(req.body.categoryId);
    if(!categories) return res.status(400).send('you have been sent invalid category');

    let supplier=await Supplier.findById(req.body.supplierId);
    if(!supplier) return res.status(400).send('you have been sent invalid supplier');

    let ouneruser=await Account.findById(req.body.accountId);
    if(!ouneruser) return res.status(400).send('you have been sent invalid user');

let product=new Product({
names:req.body.names,

ownerUser:{
    username:ouneruser.username,
    email:ouneruser.email,
    user_image:ouneruser.user_image
},

type:req.body.type,


category:{
    category:categories.category,
    description:categories.description
},


image:req.body.image,
total_price:req.body.total_price,
code:req.body.code,
quantity:req.body.quantity,

supplier:{
    names:supplier.names,
    email:supplier.email,
    phone:supplier.phone,
    city:supplier.city,
    business_name:supplier.business_name
}


});

product=await product.save();
res.send(product);



});




module.exports=router;
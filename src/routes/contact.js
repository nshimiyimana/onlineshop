const {Contact,validate}=require('../modal/contact');
const {customer, Customer}=require('../modal/customers');

const express=require('express');
const router=express.Router();




router.post('/',async (req,res)=>{
    const {error}=validate(req.body);
    if(error) return res.status(400).send(error.details[0].message);

    const customerDetails=await Customer.findById(req.body.customerId);
    
try{
let contact=new Contact({
    names:req.body.names,
    mailFrom:req.body.mailFrom,
    mailTo:req.body.mailTo,
    existance:req.body.existance,
    message:req.body.message,
    customer:{
        telephone_No:customerDetails.telephone_No,
        city:customerDetails.city,
        country:customerDetails.country,
        age:customerDetails.age


    }

});

contact=await contact.save();

res.send(contact);
}
catch(err){
    res.send(err.message);
}
});






module.exports=router;

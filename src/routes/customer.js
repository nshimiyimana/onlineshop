const {Customer,validate}=require('../modal/customers');
const validateObjectId=require('../moddleware/validateObjectId');

const express=require('express');
const { model } = require('mongoose');
const router=express.Router();



router.post('/',async (req,res)=>{
const byCustomer= Customer.find(req.body.customerId);

    
 let customer=new Customer({
     
    firstname:req.body.firstname,
    lastname:req.body.lastname,
    telephone_No:req.body.telephone_No,
    postalCode:req.body.postalCode,
    city:req.body.city,
    country:req.body.country,
    age:req.body.age


});

try{

customer=await customer.save();
res.send(customer);

}
catch(err){
    console.log(err.message);
    res.send(err.message)
}


});

router.get('/',async (req,res)=>{
    let customer=await Customer.find();
    res.send(customer);

});

router.get('/:id',validateObjectId, async (req,res)=>{
let customer=await Customer.findById(req.params.id);
if(!customer) return res.status(404).send('customer with the given id is not found');
res.send(customer);

});


router.delete('/:id',async (req,res)=>{

let customer=await Customer.findByIdAndRemove(req.params.id);
if(!customer) return res.status(404).send('customer with the given id is not found');
res.send(customer);

});


router.put('/:id',async(req,res)=>{


    let customer=await Customer.findByIdAndUpdate(req.params.id,{
        firstname:req.body.firstname,
    lastname:req.body.lastname,
    telephone_No:req.body.telephone_No,
    postalCode:req.body.postalCode,
    city:req.body.city,
    country:req.body.country,
    age:req.body.age
    },{new:true});
    res.send(customer);
})

module.exports =router;
const {Category,validate}=require('../modal/categories');
const validateObjectId=require('../moddleware/validateObjectId');
const express=require('express');
const { route } = require('./account');
const router=express.Router();


router.post('/', async (req,res)=>{

    const {error}=validate(req.body);
    if(error) return res.status(400).send(error.details[0].message);
let category=new Category({
    category:req.body.category,
    description:req.body.description
});


category=await category.save();
res.send(category);


});

router.get('/:id',validateObjectId,async (req,res)=>{
let categories=await Category.findById(req.params.id);
if(!categories) return res.status(404).send('please category with the given Id is not found');

res.send(categories);


});
router.get('/',async (req,res)=>{
    let categories=await Category.find();//select('-_id');
    
    
    res.send(categories);
});  


module.exports =router;


const mongoose=require('mongoose');



const accountSchema=new mongoose.Schema({

username:{
    type:String,
    minlength:5,
    maxlength:50,
    required:true,
    unique:true
},

password:{
    type:String,
minlength:8,
maxlength:50,
required:true

},

email:{
    type:String,
    unique:true,
    required:true,  
},

created_at:{
    type:Date,
    default:Date.now
},

user_image:String,

isAdmin:{
    type:Boolean,
    default:false
},

operations:{
    type:String,
    enum:['read','write','delete','update'],
    default:'read'

}



});



const Account=mongoose.model('account',accountSchema);


module.exports.Account=Account;
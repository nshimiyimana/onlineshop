const Joi=require('joi');
const mongoose=require('mongoose');
const orderSchema=new mongoose.Schema({
    orderName:{
        type:String,
        required:true,
        match:/.buy */i,
             },
             product:new mongoose.Schema(
                 {
names:String,
type:String,
image:String,
code:String,
total_price:Number,
quantity:Number                 }
             ),

             order_date:{
                 type:Date,
                 default:Date.now,
             },
productOwner:new mongoose.Schema({
    username:{
        type:String,
        required:true
    },
    email:{
    type:String,
    match:/.*@*.com/i,
    required:true
    },
    user_image:{
        type:String,
        trim:true,
        required:true
    }
}),

             customer:new mongoose.Schema({
                 names:String,
                 telephone_No:String,
                 postal_code:String,
                 city:String,
                 age:Number
             })

});


const Order=mongoose.model('order',orderSchema);

function validateOrder(order){
    const schema={
        orderName:Joi.string().required(),
        productId:Joi.objectId().required(),
        accountId:Joi.objectId().required(),
        customerId:Joi.objectId().required()
    };
    return Joi.validate(order,schema);
}


module.exports.Order=Order;
module.exports.validate=validateOrder;
const Joi=require('joi');
const mongoose=require('mongoose');


const contactSchema=new mongoose.Schema({
names:{
    type:String,
    maxlength:50,
    minlength:4,
    required:true,
    true:true
},
mailFrom:{
    type:String,
    match:/.*@*.com/i,
    maxlength:50,
    required:true
},
mailTo:{
    type:String,
    match:/.*@*.com/i,
    maxlength:50,
    required:true
},
existance:{
    type:String,
    enum:['yes','no'],
    default:'no',

},
message:{
    type:String,
    required:true,
    trim:true
},
customer:{
    type:new mongoose.Schema({
        telephone_No:String,
        city:String,
        country:String,
        age:Number,

    })

}

});

const Contact=mongoose.model('contact',contactSchema);


function validateContact(contact){

const schema={
    names:Joi.string().min(5).max(50).required(),
    mailFrom:Joi.string().max(50).email().required(),
    mailTo:Joi.string().max(50).email().required(),
    existance:Joi.string().default('no').min(2).max(3),
    message:Joi.string().required(),
   customerId:Joi.objectId().required()
};
return Joi.validate(contact,schema);

}

module.exports.Contact=Contact;
module.exports.validate=validateContact;
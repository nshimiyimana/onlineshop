

const mongoose=require('mongoose');



const categorySchema=new mongoose.Schema({
    category:{
        type:String,
        enum:['shoes','computer technology','clothes','shoes','cleanes','cook ware'],
        required:true

    },
    description:{
        type:String,
        text:true,
        minlength:10,
        trim:true
    }
});


const Category=mongoose.model('category',categorySchema);


function validateCategory(category){

    const schema={
category:Joi.string().required(),
description:Joi.string().trim().min(20).required()
    };
    return Joi.validate(category,schema);
}

module.exports.Category=Category;
module.exports.validate=validateCategory;
module.exports.categorySchema=categorySchema;


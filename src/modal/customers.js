const Joi=require('joi');

const mongoose=require('mongoose');




const customerSchema=new mongoose.Schema({
    
    firstname:{
      type: String,
      uppercase:true
    },
    lastname:String,
    telephone_No:{
        type:String,
        maxlength:15,
        minlength:10,
        required:true
    },
    postalCode:{
       type:String,
       required:true
    },
    city:{
        type:String,
        required:true
    },
    country:{
       type:String,
       uppercase:true,
       required:true
    },
    age:{
type:Number,
max:150,
min:18
    }


});


const Customer=mongoose.model('customer',customerSchema);



function validateCustomer(customer){

const schema={
    firstname:Joi.string().required(),
    lastname:Joi.string().required(),
    telephone_No:Joi.string().max(15).min(10).required(),
    postalCode:Joi.string().required(),
    city:Joi.string().required(),
    country:Joi.string().required(),
};

return Joi.validate(customer,schema);



}

module.exports.Customer=Customer;
module.exports.validate=validateCustomer;
const Joi=require('joi');
const mongoose=require('mongoose');
const supplierSchema=new mongoose.Schema({
    names:String,
    email:{
        type:String,
        match:/.*@*.com/i,
        min:11,
        required:true
    },
    business_name:{
        type:String,
        required:true
    }
    ,
    phone:{
        type:String,
        max:15,
        min:10,
        trim:true
    },
    city:{
        type:String,
        required:true,

    },
    country:{
        type:String,
        required:true
    }

});


const Supplier=mongoose.model('supplier',supplierSchema);


function validateSupplier(supplier){
    const schema={
names:Joi.string().required(),
email:Joi.string().email().required(),
business_name:Joi.string().required(),
city:Joi.string().required(),
country:Joi.string().required()
    };

    return Joi.validate(supplier,schema);
}

module.exports.Supplier=Supplier;
module.exports.validate=validateSupplier;
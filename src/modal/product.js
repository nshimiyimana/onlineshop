const Joi=require('joi');



const {categorySchema}=require('./categories');

const mongoose=require('mongoose');

const productSchema=new mongoose.Schema({
    names:{
        type:String,
        required:true,
        trim:true
    },
    ownerUser:new mongoose.Schema({

username:{
    type:String,
    required:true
},
email:{
type:String,
match:/.*@*.com/i,
required:true
},
user_image:{
    type:String,
    trim:true,
    required:true
}


    }),
    type:{
        type:String,
        required:true
    },
    category:new mongoose.Schema({
        category:String,
        description:String
    }),
    image:{
        type:String,
  required:true
          },
         total_price:{
              type:Number,
              required:true,
              trim:true
            },
          code:{
             type: String,
             trim:true,
             required:true
          },
          quantity:{
              type:Number,
              trim:true,
              default:1,
              required:true
          },
          supplier:new mongoose.Schema({
              names:String,
              email:String,
              phone:String,
              city:String,
              business_name:String
          })

});

const Product=mongoose.model('online_products',productSchema);


function validateProduct(product){
    const schema={
        names:Joi.string().required(),
        accountId:Joi.objectId().required(),
        type:Joi.string().required(),
        categoryId:Joi.objectId().required(),
        image:Joi.string().required(),
        total_price:Joi.number().required(),
        code:Joi.string().required(),
        quantity:Joi.number().required(),
        supplierId:Joi.objectId().required()
        
    };


return Joi.validate(product,schema);
}

module.exports.Product=Product;
module.exports.validate=validateProduct;
const account=require('../routes/account');
const customer=require('../routes/customer');
const contact=require('../routes/contact');
const categories=require('../routes/category');
const supplier=require('../routes/supplier');
const products=require('../routes/products');
//const balance=require('../routes/balance');

const express=require('express');

module.exports=function(app){

app.use(express.json());
app.use('/api/account',account);
app.use('/api/customers',customer);
app.use('/api/contacts',contact);
app.use('/api/categories',categories);
app.use('/api/suppliers',supplier);
app.use('/api/products',products);
}
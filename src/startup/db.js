const mongoose=require('mongoose');
const config=require('config');



module.exports=function(){
    const db=config.get("db"); //this configured in config folder in jsons file the purpose is to run integration test 
    mongoose.connect(db)
.then(()=>console.log('successfullly connected to mongodb'))
.catch(()=>console.log('unable to establish connection with mongodb'));

}
